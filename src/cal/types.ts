import { INestedObj } from "../common";
interface ITimeFrame {
    timeFrom: number
    timeTo: number
}
interface ICalEvent {
    theme: INestedObj,
    style: {
        gridColumn: number,
        gridRow: string
    },
    text: string,
    timeFrame: ITimeFrame
}
interface ICurrentTime {
    dt: number,
    gridColumn: number,
    gridRow: number,
    first: number,
    last: number,
    top: string
    month: string
    year: string | number
    date: string | number
    week: {isCurrent: boolean, name: string, day: number} []
}

interface IJSEvent {
    text: string
    theme: INestedObj,
    timeFrom: number
    timeTo: number
}
export type {
    ICalEvent,
    ICurrentTime,
    IJSEvent,
    ITimeFrame,
}
