import {IFluxAction, Optional} from "./types";

const addCustomPolyFills = () => {
    // custom polyfils
}

function defaultStateReducer(state: any, action: IFluxAction) {
    const { payload, type: actionType } = action
    const newState = { ...state, ...payload }
    const updatedState: Optional<any> = {}

    switch (actionType) {
        default:
            break
    }
    return {
        ...newState,
        ...updatedState,
    }
}

export {
    addCustomPolyFills,
    defaultStateReducer,
}
