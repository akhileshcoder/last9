import { Typography } from '@material-ui/core'
import * as React from 'react'
import { Dialog } from '../dialogs'
import { OutlineLinkButtonPrimary as LinkButton } from '../button'
import { ReactChildren } from '../../types'
import { useStyles } from './style'

interface IProps {
    open: boolean
    maxLength?: number
    minLength?: number
    onClose?: () => void
    onCancel?: () => void
    onSubmit?: () => void
    subTitle: string | JSX.Element
    submitText?: string | JSX.Element
    title: string | JSX.Element
    value?: string
    children: ReactChildren
}
// tslint:disable-next-line: no-empty
const defaultCallBack = () => {}
function FormPopUp({ open, title, subTitle, submitText = 'Submit', onCancel = defaultCallBack, onClose = defaultCallBack, onSubmit = defaultCallBack, children }: IProps) {
    const classes = useStyles()
    return (
        <Dialog aria-labelledby="Submit your popUp" aria-describedby="Select close to close dialog." open={open} onClose={onClose}>
            <div className={classes.popUpDialog}>
                <Typography className={classes.popupTitle}>
                    {title}
                </Typography>
                {subTitle ? (
                    <Typography className={classes.popupSubTitle}>
                        {subTitle}
                    </Typography>
                ) : null}
                {children}
                <div className={classes.popUpAction}>
                    <LinkButton
                        className={classes.popUpBtn}
                        textPrimary={true}
                        onClick={onCancel}
                        label={submitText}
                    />
                    <LinkButton
                        className={classes.popUpBtn}
                        textPrimary={true}
                        onClick={onSubmit}
                        label="Cancel"
                    />
                </div>
            </div>
        </Dialog>
    )
}

export { FormPopUp }
